package guy.droid.im.launcherapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by admin on 1/4/2017.
 */

public class Booter extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context,"BOOTED",Toast.LENGTH_LONG).show();
        context.startActivity(new Intent(context,MainActivity.class));
    }
}
